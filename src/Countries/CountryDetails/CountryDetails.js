import React from 'react';

const CountryDetails = (props) => {

  return (
    <div className='details'>
      <div className='name'>
        <h2>{props.country.name}</h2>
        <p>Capital: {props.country.capital}</p>
        <p>Population: {props.country.population}</p>
      </div>
      <div style={{float:'right'}}><img className='flag' src={props.country.flag} alt=""/></div>
      <div className='borders'>
        <h3>Borders with: </h3>
        <ul>
          {
            props.borders.map(border => {
              return  <li key={border}>
                <p  className="text">{border}</p>
              </li>
            })
          }
        </ul>
      </div>

    </div>
  );

};

export default CountryDetails;