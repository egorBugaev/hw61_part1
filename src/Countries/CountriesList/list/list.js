import React from 'react';

const list = props => {
  return (
    <li >
      <p onClick={() => props.select(props.name)} className="text">{props.name}</p>
    </li>
  )
};

export default list;