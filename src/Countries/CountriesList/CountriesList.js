import React from 'react';
import Message from "./list/list";

const CountryList = props => {
  return (
    <div className='countryList'>
      <ul id="list">
        {
          props.country.map(country => {
            return <Message
              name={country.name}
              key={country.alpha3Code}
              select={props.select}
            />
          })
        }
      </ul>
    </div>
  )
};

export default CountryList;