import React, {Component} from 'react';
import axios from 'axios';
import CountryList from "./CountriesList/CountriesList";
import CountryDetails from "./CountryDetails/CountryDetails";

class Countries extends Component {

  state = {
    allCountries: [],
    selectedCountry: '',
    borders: []
  };
  getCountries = async () => {
    try {
      const allCountries = [...this.state.allCountries];
      let country = await axios.get(`https://restcountries.eu/rest/v2/all`);

      country.data.forEach(country => allCountries.push(country));

      this.setState({allCountries});
    } catch (error) {
      console.log(error);
    }
  };

  componentDidMount() {
    this.getCountries();
  }

  select = (name) => {
    const allCountries = [...this.state.allCountries];
    const borders = [];
    const index = allCountries.findIndex(p => p.name === name);
    console.log(allCountries[index]);
    for (let i = 0; i < allCountries[index].borders.length; i++) {
      let bord = allCountries.findIndex(p => p.alpha3Code === allCountries[index].borders[i])
      borders.push(this.state.allCountries[bord].name);
    }
    this.setState({selectedCountry: allCountries[index], borders});
  };

  render() {

    return (
      <div>
        <CountryList
          country={this.state.allCountries}
          select={this.select}
        />
        <CountryDetails
          country={this.state.selectedCountry}
          borders={this.state.borders}
        />
      </div>
    );
  }

}

export default Countries